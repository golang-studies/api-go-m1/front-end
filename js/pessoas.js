let modalMessageAlert, listPessoas, tableBody;
const restApi = axios.create({
    baseURL: 'http://127.0.0.1:4000',
    timeout: 30000
});


function initPessoas() {
    const input = document.getElementById("inpDataSearch");
    input.focus();

    modalMessageAlert = document.getElementById('modalMessageAlert');
    tableBody = document.getElementById('tabelaPessoas').lastElementChild;
}

function alterarConteudoDiv(button) {
    let dt_type = button.getAttribute("data-type");

    const divPesquisa = document.getElementById("pesquisarPessoas");
    const divCadastro = document.getElementById("cadastroPessoas");
    if (dt_type == 0) {
        divPesquisa.classList.remove("d-none");
        divCadastro.classList.add("d-none");
        button.textContent = "Cadastrar"

        const input = document.getElementById("inpDataSearch");
        input.focus();

        tableBody.innerHTML = ""
    } else {
        divCadastro.classList.remove("d-none");
        divPesquisa.classList.add("d-none");
        button.textContent = "Pesquisar"

        const input = document.getElementById("NomePessoa");
        input.focus();
    }

    dt_type = 1 - dt_type;
    button.setAttribute("data-type", dt_type);
}

function alterarLegendaStatus(input) {
    const label = document.getElementById("labelCheckStatus");
    if (input.checked) {
        label.textContent = "Ativado";
    } else {
        label.textContent = "Desativado";
    }
}

function closeModalMessageAlert() {  
    modalMessageAlert.classList.remove("show");
    modalMessageAlert.style.display = "none";

    const modalContent = modalMessageAlert.firstElementChild.firstElementChild;
    modalContent.classList.remove(openModalMessageAlert.borderClass);
    const modalHeader = modalContent.firstElementChild;
    modalHeader.classList.remove(openModalMessageAlert.bgClass);
}

function openModalMessageAlert(texto,bgClass,borderClass) {
    const modalContent = modalMessageAlert.firstElementChild.firstElementChild;
    modalContent.classList.add(borderClass);
    const modalHeader = modalContent.firstElementChild;
    modalHeader.classList.add(bgClass);

    const mdlMsgAlertInfo = document.getElementById('modalMessageAlertInfo');
    mdlMsgAlertInfo.innerHTML = texto;

    openModalMessageAlert.borderClass = borderClass;
    openModalMessageAlert.bgClass = bgClass;

    modalMessageAlert.classList.add("show");
    modalMessageAlert.style.display = "block";
}

function salvarPessoa(button) {
    const inpPessoa = document.getElementById('IdPessoa');
    const inpName = document.getElementById('NomePessoa');
    const inpEmail = document.getElementById('EmailPessoa');
    const inpIdade = document.getElementById('IdadePessoa');
    const inpChecked = document.getElementById('checkStatus');

    const data = {
        id: inpPessoa.value,
        nome: inpName.value,
        email: inpEmail.value,
        idade: inpIdade.value,
        status: inpChecked.checked
    }

    var PessoaID = 0;
    if (inpPessoa.value.length > 0) {
        PessoaID = parseInt(inpPessoa.value);
    }

    const span = button.firstElementChild;
    span.classList.remove("d-none");
    if (PessoaID < 1) {
        restApi.post('/adm/pessoas', data)
            .then(response => {
                const person = response.data;    
                openModalMessageAlert("Salvo com sucesso!","bg-success", "border-success");
                inpPessoa.value = person.Id;
                setTimeout(() => {                
                    span.classList.add("d-none");
                }, 300);
            })
            .catch(error => {
                const dataErr = error.response.data;
    
                openModalMessageAlert(dataErr.Mensagem,"bg-warning", "border-warning");
                setTimeout(() => {                
                    span.classList.add("d-none");
                }, 300);
            });
    } else {
        restApi.patch('/adm/pessoas', data)
            .then(response => {  
                openModalMessageAlert("Alterado com sucesso!","bg-success", "border-success");
                setTimeout(() => {                
                    span.classList.add("d-none");
                }, 300);
            })
            .catch(error => {
                const dataErr = error.response.data;
    
                openModalMessageAlert(dataErr.Mensagem,"bg-warning", "border-warning");
                setTimeout(() => {                
                    span.classList.add("d-none");
                }, 300);
            });
    }
}

function novaPessoa(button) {
    document.getElementById('IdPessoa').value = "";
    document.getElementById('NomePessoa').value = "";
    document.getElementById('EmailPessoa').value = "";
    document.getElementById('IdadePessoa').value = "";
    document.getElementById('checkStatus').checked = false;
    document.getElementById('labelCheckStatus').textContent = "Desativado";
}

function editarPessoas(key) {
    console.log(listPessoas[key]);
    document.getElementById('IdPessoa').value = listPessoas[key].Id;
    document.getElementById('NomePessoa').value = listPessoas[key].Nome;
    document.getElementById('EmailPessoa').value = listPessoas[key].Email;
    document.getElementById('IdadePessoa').value = listPessoas[key].Idade;

    if (listPessoas[key].Status > 0){
        document.getElementById('checkStatus').checked = false;
        document.getElementById('labelCheckStatus').textContent = "Desativado";
    } else {
        document.getElementById('checkStatus').checked = true;
        document.getElementById('labelCheckStatus').textContent = "Ativado";
    }
    
    setTimeout(() => {
        const button = document.getElementById('cadastrarPesquisar'); 
        let dt_type = button.getAttribute("data-type");
        const divPesquisa = document.getElementById("pesquisarPessoas");
        const divCadastro = document.getElementById("cadastroPessoas");
        divCadastro.classList.remove("d-none");
        divPesquisa.classList.add("d-none");
        button.textContent = "Pesquisar"
    
        const input = document.getElementById("NomePessoa");
        input.focus();
        dt_type = 1 - dt_type;
        button.setAttribute("data-type", dt_type);
    }, 200);
}

function montaTabelaPessoas(pessoas) {
    let bodyInf = ""; 
    for (const key in pessoas) {
        if (Object.hasOwnProperty.call(pessoas, key)) {
            const element = pessoas[key];

            bodyInf += "<tr onclick='editarPessoas("+ key +")' style='cursor: pointer;'>";
            bodyInf += "<th scope='row'>"+element.Id+"</th>";
            bodyInf += "<th>"+element.Nome+"</th>";
            bodyInf += "<th>"+element.Email+"</th>";
            bodyInf += "<th>"+element.Idade+"</th>";
            
            if (element.Status > 0){
                bodyInf += "<th><span class='badge bg-danger'>Desativado</span></th>";
            } else {
                bodyInf += "<th><span class='badge bg-success'>Ativado</span></th>";
            }
            bodyInf += "</tr>";
        }
    }
    tableBody.innerHTML = bodyInf;
}

function searchPessoas(button) {
    const span = button.firstElementChild;
    span.classList.remove("d-none");

    const inpName = document.getElementById('inpDataSearch');
    const data = {
        nome: inpName.value
    };
    console.log(data);
    tableBody.innerHTML = "";
    
    restApi.get('/src/pessoas', 
        { 
            params: { 
                source_content_type: 'application/json',
                source: JSON.stringify(data)
            } 
        }
    )
        .then(response => {
            listPessoas = response.data.Pessoas;
            
            montaTabelaPessoas(listPessoas);
            setTimeout(() => {                
                span.classList.add("d-none");
            }, 400);
        })
        .catch(error => {
            const dataErr = error.response.data;

            openModalMessageAlert(dataErr.Mensagem,"bg-warning", "border-warning");
            setTimeout(() => {                
                span.classList.add("d-none");
            }, 300);
        });
}