let modalMessageAlert, listProdutos, tableBody;
const restApi = axios.create({
    baseURL: 'http://127.0.0.1:4000',
    timeout: 30000
});

function initProdutos() {
    const input = document.getElementById("inpDataSearch");
    input.focus();

    modalMessageAlert = document.getElementById('modalMessageAlert');
    tableBody = document.getElementById('tabelaProdutos').lastElementChild;
}

function alterarConteudoDiv(button) {
    let dt_type = button.getAttribute("data-type");

    const divPesquisa = document.getElementById("pesquisarProdutos");
    const divCadastro = document.getElementById("cadastroProdutos");
    if (dt_type == 0){
        divPesquisa.classList.remove("d-none");
        divCadastro.classList.add("d-none");
        button.textContent = "Cadastrar"

        const input = document.getElementById("inpDataSearch");
        input.focus();
        tableBody.innerHTML = ""
    }else{
        divCadastro.classList.remove("d-none");
        divPesquisa.classList.add("d-none");
        button.textContent = "Pesquisar"

        const input = document.getElementById("DescricaoProdutos");
        input.focus();
    }
    
    dt_type = 1 - dt_type;
    button.setAttribute("data-type", dt_type);
}

function alterarLegendaStatus(input) {
    const label = document.getElementById("labelCheckStatus");  
    if (input.checked) {
        label.textContent = "Ativado";
    }else{
        label.textContent = "Desativado";
    }
}

function closeModalMessageAlert() {  
    modalMessageAlert.classList.remove("show");
    modalMessageAlert.style.display = "none";

    const modalContent = modalMessageAlert.firstElementChild.firstElementChild;
    modalContent.classList.remove(openModalMessageAlert.borderClass);
    const modalHeader = modalContent.firstElementChild;
    modalHeader.classList.remove(openModalMessageAlert.bgClass);
}

function openModalMessageAlert(texto,bgClass,borderClass) {
    const modalContent = modalMessageAlert.firstElementChild.firstElementChild;
    modalContent.classList.add(borderClass);
    const modalHeader = modalContent.firstElementChild;
    modalHeader.classList.add(bgClass);

    const mdlMsgAlertInfo = document.getElementById('modalMessageAlertInfo');
    mdlMsgAlertInfo.innerHTML = texto;

    openModalMessageAlert.borderClass = borderClass;
    openModalMessageAlert.bgClass = bgClass;

    modalMessageAlert.classList.add("show");
    modalMessageAlert.style.display = "block";
}

function salvarProduto(button) {
    const inpProduto = document.getElementById('IdProduto');
    const inpDescricao = document.getElementById('DescricaoProdutos');
    const inpValor = document.getElementById('ValorProduto');
    const inpChecked = document.getElementById('checkStatus');

    const data = {
        id: inpProduto.value,
        descricao: inpDescricao.value,
        valor: inpValor.value,
        status: inpChecked.checked
    }

    let ProdutoID = 0;
    if (inpProduto.value.length > 0) {
        ProdutoID = parseInt(inpProduto.value);
    }

    const span = button.firstElementChild;
    span.classList.remove("d-none");
    if (ProdutoID < 1) {
        restApi.post('/adm/produtos', data)
            .then(response => {
                const person = response.data;    
                openModalMessageAlert("Salvo com sucesso!","bg-success", "border-success");
                inpProduto.value = person.Id;
                setTimeout(() => {                
                    span.classList.add("d-none");
                }, 300);
            })
            .catch(error => {
                const dataErr = error.response.data;
    
                openModalMessageAlert(dataErr.Mensagem,"bg-warning", "border-warning");
                setTimeout(() => {                
                    span.classList.add("d-none");
                }, 300);
            });
    } else {
        restApi.patch('/adm/produtos', data)
            .then(response => {  
                openModalMessageAlert("Alterado com sucesso!","bg-success", "border-success");
                setTimeout(() => {                
                    span.classList.add("d-none");
                }, 300);
            })
            .catch(error => {
                const dataErr = error.response.data;
    
                openModalMessageAlert(dataErr.Mensagem,"bg-warning", "border-warning");
                setTimeout(() => {                
                    span.classList.add("d-none");
                }, 300);
            });
    }
}

function novoProduto(button) {
    document.getElementById('IdProduto').value = "";
    document.getElementById('DescricaoProdutos').value = "";
    document.getElementById('ValorProduto').value = "";
    document.getElementById('checkStatus').checked = false;
    document.getElementById('labelCheckStatus').textContent = "Desativado";
}

function editarProdutos(key) {
    console.log(listProdutos[key]);
    document.getElementById('IdProduto').value = listProdutos[key].Id;
    document.getElementById('DescricaoProdutos').value = listProdutos[key].Descricao;
    document.getElementById('ValorProduto').value = listProdutos[key].Valor;
    
    if (listProdutos[key].Status > 0){
        document.getElementById('checkStatus').checked = false;
        document.getElementById('labelCheckStatus').textContent = "Desativado";
    } else {
        document.getElementById('checkStatus').checked = true;
        document.getElementById('labelCheckStatus').textContent = "Ativado";
    }
    
    setTimeout(() => {
        const button = document.getElementById('cadastrarPesquisar'); 
        let dt_type = button.getAttribute("data-type");
        const divPesquisa = document.getElementById("pesquisarProdutos");
        const divCadastro = document.getElementById("cadastroProdutos");
        divCadastro.classList.remove("d-none");
        divPesquisa.classList.add("d-none");
        button.textContent = "Pesquisar"
    
        const input = document.getElementById("DescricaoProdutos");
        input.focus();
        dt_type = 1 - dt_type;
        button.setAttribute("data-type", dt_type);
        tableBody.innerHTML = "";
    }, 200);
}

function montaTabelaProdutos(produtos) {
    let bodyInf = ""; 
    for (const key in produtos) {
        if (Object.hasOwnProperty.call(produtos, key)) {
            const element = produtos[key];

            bodyInf += "<tr onclick='editarProdutos("+ key +")' style='cursor: pointer;'>";
            bodyInf += "<th scope='row'>"+element.Id+"</th>";
            bodyInf += "<th>"+element.Descricao+"</th>";
            bodyInf += "<th>"+element.Valor+"</th>";
        
            if (element.Status > 0){
                bodyInf += "<th><span class='badge bg-danger'>Desativado</span></th>";
            } else {
                bodyInf += "<th><span class='badge bg-success'>Ativado</span></th>";
            }
            bodyInf += "</tr>";
        }
    }
    tableBody.innerHTML = bodyInf;
}

function searchProdutos(button) {
    const span = button.firstElementChild;
    span.classList.remove("d-none");

    const inpName = document.getElementById('inpDataSearch');
    const data = {
        descricao: inpName.value
    };
    console.log(data);
    tableBody.innerHTML = "";
    
    restApi.get('/src/produtos', 
        { 
            params: { 
                source_content_type: 'application/json',
                source: JSON.stringify(data)
            } 
        }
    )
        .then(response => {
            listProdutos = response.data.Produtos;
            
            montaTabelaProdutos(listProdutos);
            setTimeout(() => {                
                span.classList.add("d-none");
            }, 400);
        })
        .catch(error => {
            const dataErr = error.response.data;

            openModalMessageAlert(dataErr.Mensagem,"bg-warning", "border-warning");
            setTimeout(() => {                
                span.classList.add("d-none");
            }, 300);
        });
}